'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var videojs = _interopDefault(require('video.js'));

var version = "1.1.0";

var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};











var inherits = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
};











var possibleConstructorReturn = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
};

var MenuItem = videojs.getComponent('MenuItem');

var SourceMenuItem = function (_MenuItem) {
  inherits(SourceMenuItem, _MenuItem);

  function SourceMenuItem(player, options) {
    classCallCheck(this, SourceMenuItem);

    options.selectable = true;
    options.multiSelectable = false;

    var _this = possibleConstructorReturn(this, _MenuItem.call(this, player, options));

    _this.player().qualityLevels().on('change', _this.update.bind(_this));
    return _this;
  }

  SourceMenuItem.prototype.handleClick = function handleClick() {
    var selected = this.options_;
    videojs.log.debug("Changing quality to:", selected.label);

    this.selected(true);

    var levels = this.player().qualityLevels();
    for (var i = 0; i < levels.length; i++) {
      if (selected.index == levels.length) {
        // If this is the Auto option, enable all renditions for adaptive selection
        levels[i].enabled = true;
      } else if (selected.index == i) {
        levels[i].enabled = true;
      } else {
        levels[i].enabled = false;
      }
    }
  };

  SourceMenuItem.prototype.update = function update() {
    var levels = this.player().qualityLevels();
    var selection = levels.selectedIndex;
    this.selected(this.options_.index == selection);
  };

  return SourceMenuItem;
}(MenuItem);

var MenuButton = videojs.getComponent('MenuButton');

var SourceMenuButton = function (_MenuButton) {
  inherits(SourceMenuButton, _MenuButton);

  function SourceMenuButton(player, options) {
    classCallCheck(this, SourceMenuButton);

    var _this = possibleConstructorReturn(this, _MenuButton.call(this, player, options));

    MenuButton.apply(_this, arguments);

    var qualityLevels = _this.player().qualityLevels();

    // Handle options: We accept an options.default value of ( high || low )
    // This determines a bias to set initial resolution selection.
    if (options && options.default) {
      if (options.default == 'low') {
        for (var i = 0; i < qualityLevels.length; i++) {
          qualityLevels[i].enabled = i == 0;
        }
      } else if (options.default = 'high') {
        for (var i = 0; i < qualityLevels.length; i++) {
          qualityLevels[i].enabled = i == qualityLevels.length - 1;
        }
      }
    }

    _this.showAutoItem = options && options.showAutoItem;

    // Bind update to qualityLevels changes
    //this.player().qualityLevels.on(['change', 'addqualitylevel'], videojs.bind( this, this.update) );
    return _this;
  }

  SourceMenuButton.prototype.createEl = function createEl() {
    return videojs.dom.createEl('div', {
      className: 'vjs-http-source-selector vjs-menu-button vjs-menu-button-popup vjs-control vjs-button'
    });
  };

  SourceMenuButton.prototype.buildCSSClass = function buildCSSClass() {
    return MenuButton.prototype.buildCSSClass.call(this) + ' vjs-icon-cog';
  };

  SourceMenuButton.prototype.update = function update() {
    return MenuButton.prototype.update.call(this);
  };

  SourceMenuButton.prototype.createItems = function createItems() {
    var menuItems = [];
    var levels = this.player().qualityLevels();
    var labels = [];

    for (var i = 0; i < levels.length; i++) {
      var index = levels.length - (i + 1);
      var level = levels[index];
      var selected = index === levels.selectedIndex;
      // TODO(Leon Handreke): This is a bit evil because it adds a direct dependency on http-streaming. However, we
      //  don't have another choice currently since otherwise we would need to propagate the playlist attributes through
      //  to the representations, which would require changes to video.js. Also, these changes are pretty specific
      //  to us, since we're communicating metadata of the stream that is very specific to us (whether the stream is
      //  transmuxed or transcoded on the server) in the name field.
      var playlist = this.player_.tech().hls.playlists.master.playlists[levels[index].id];

      var label = '' + index;
      var sortVal = index;
      if (level.bitrate) {
        // Round to 0.1 Mbps
        label = Math.floor(level.bitrate / 1e5) / 1e1 + ' Mbps';
        sortVal = level.bitrate;
      }
      if (playlist.attributes["NAME"] === "direct") {
        if (label !== '') {
          label += ' / ';
        }
        label += "Transmux";
      } else if (level.height) {
        if (label !== '') {
          label += ' / ';
        }
        label += level.height + 'p';
        //sortVal = parseInt(levels[index].height, 10)
      }

      // Skip duplicate labels
      if (labels.indexOf(label) >= 0) {
        continue;
      }
      labels.push(label);

      menuItems.push(new SourceMenuItem(this.player_, { label: label, index: index, selected: selected, sortVal: sortVal }));
    }

    // If there are multiple quality levels, offer an 'auto' option
    if (this.showAutoItem && levels.length > 1) {
      menuItems.push(new SourceMenuItem(this.player_, { label: 'Auto', index: levels.length, selected: false, sortVal: 99999 }));
    }

    // Sort menu items by their label name with Auto always first
    menuItems.sort(function (a, b) {
      return b.options_.sortVal - a.options_.sortVal;
    });

    return menuItems;
  };

  return SourceMenuButton;
}(MenuButton);

// Default options for the plugin.
var defaults = {};

// Cross-compatibility for Video.js 5 and 6.
var registerPlugin = videojs.registerPlugin || videojs.plugin;

/**
* Function to invoke when the player is ready.
*
* This is a great place for your plugin to initialize itself. When this
* function is called, the player will have its DOM and child components
* in place.
*
* @function onPlayerReady
* @param    {Player} player
*           A Video.js player object.
*
* @param    {Object} [options={}]
*           A plain object containing options for the plugin.
*/
var onPlayerReady = function onPlayerReady(player, options) {
  //This plugin only supports level selection for HLS playback
  if (player.techName_ != 'Html5') {
    return;
  }

  player.addClass('vjs-http-source-selector');

  /**
  * We have to wait for the manifest to load before we can scan renditions for resolutions/bitrates to populate selections
  **/
  player.on(['loadedmetadata'], function (e) {
    // Initialize qualityLevels plugin
    player.qualityLevels();

    // Hack to make initialization idempotent
    if (player.videojs_http_source_selector_initialized !== true) {
      player.videojs_http_source_selector_initialized = true;
      player.getChild('controlBar').addChild('SourceMenuButton', options);
    }
  });
};

/**
 * A video.js plugin.
 *
 * In the plugin function, the value of `this` is a video.js `Player`
 * instance. You cannot rely on the player being in a "ready" state here,
 * depending on how the plugin is invoked. This may or may not be important
 * to you; if not, remove the wait for "ready"!
 *
 * @function httpSourceSelector
 * @param    {Object} [options={}]
 *           An object of options left to the plugin author to define.
 */
var httpSourceSelector = function httpSourceSelector(options) {
  var _this = this;

  this.ready(function () {
    onPlayerReady(_this, videojs.mergeOptions(defaults, options));
  });

  videojs.registerComponent('SourceMenuButton', SourceMenuButton);
  videojs.registerComponent('SourceMenuItem', SourceMenuItem);
};

// Register the plugin with video.js.
registerPlugin('httpSourceSelector', httpSourceSelector);

// Include the version number.
httpSourceSelector.VERSION = version;

module.exports = httpSourceSelector;
