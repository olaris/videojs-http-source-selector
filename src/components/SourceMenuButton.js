import videojs from 'video.js';

import SourceMenuItem from './SourceMenuItem';

const MenuButton = videojs.getComponent('MenuButton');

class SourceMenuButton extends MenuButton
{
  constructor(player, options) {
    super(player, options);

    MenuButton.apply(this, arguments);

    let qualityLevels = this.player().qualityLevels();

    // Handle options: We accept an options.default value of ( high || low )
    // This determines a bias to set initial resolution selection.
    if (options && options.default) {
      if (options.default == 'low') {
        for (var i = 0; i < qualityLevels.length; i++) {
          qualityLevels[i].enabled = (i == 0);
        }
      } else if (options.default = 'high') {
        for (var i = 0; i < qualityLevels.length; i++) {
          qualityLevels[i].enabled = (i == (qualityLevels.length - 1));
        }
      }
    }

    this.showAutoItem = options && options.showAutoItem

    // Bind update to qualityLevels changes
    //this.player().qualityLevels.on(['change', 'addqualitylevel'], videojs.bind( this, this.update) );
  };

  createEl() {
    return videojs.dom.createEl('div', {
      className: 'vjs-http-source-selector vjs-menu-button vjs-menu-button-popup vjs-control vjs-button'
    });
  }

  buildCSSClass() {
    return MenuButton.prototype.buildCSSClass.call( this ) + ' vjs-icon-cog';
  }

  update() {
    return MenuButton.prototype.update.call(this);
  }

  createItems() {
    let menuItems = [];
    const levels = this.player().qualityLevels();
    let labels = [];

    for (var i = 0; i < levels.length; i++) {
      var index = levels.length - (i + 1);
      var level = levels[index];
      var selected = (index === levels.selectedIndex);
      // TODO(Leon Handreke): This is a bit evil because it adds a direct dependency on http-streaming. However, we
      //  don't have another choice currently since otherwise we would need to propagate the playlist attributes through
      //  to the representations, which would require changes to video.js. Also, these changes are pretty specific
      //  to us, since we're communicating metadata of the stream that is very specific to us (whether the stream is
      //  transmuxed or transcoded on the server) in the name field.
      var playlist = this.player_.tech().hls.playlists.master.playlists[levels[index].id];

      var label = `${index}`;
      var sortVal = index;
      if (level.bitrate) {
        // Round to 0.1 Mbps
        label = `${Math.floor(level.bitrate / 1e5) / 1e1} Mbps`;
        sortVal = level.bitrate
      }
      if (playlist.attributes["NAME"] === "direct") {
        if (label !== '') {
          label += ' / '
        }
        label += "Transmux"
      } else if (level.height) {
        if (label !== '') {
          label += ' / '
        }
        label += `${level.height}p`;
        //sortVal = parseInt(levels[index].height, 10)
      }

      // Skip duplicate labels
      if (labels.indexOf(label) >= 0) {
        continue
      }
      labels.push(label);

      menuItems.push(new SourceMenuItem(this.player_, { label, index, selected, sortVal }));
    }

    // If there are multiple quality levels, offer an 'auto' option
    if (this.showAutoItem && levels.length > 1) {
      menuItems.push(new SourceMenuItem(this.player_, { label: 'Auto', index: levels.length, selected: false, sortVal: 99999 }));
    }

    // Sort menu items by their label name with Auto always first
    menuItems.sort((a, b) => b.options_.sortVal - a.options_.sortVal);

    return menuItems;
  }
}

export default SourceMenuButton;
