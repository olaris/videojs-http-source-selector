import videojs from 'video.js';

const MenuItem = videojs.getComponent('MenuItem');

class SourceMenuItem extends MenuItem
{
  constructor(player, options) {
    options.selectable = true;
    options.multiSelectable = false;

    super(player, options);

    this.player().qualityLevels().on('change', this.update.bind(this))
  }

  handleClick() {
    let selected = this.options_;
    videojs.log.debug("Changing quality to:", selected.label);

    this.selected(true);

    let levels = this.player().qualityLevels();
    for(var i = 0; i < levels.length; i++) {
      if (selected.index == levels.length) {
        // If this is the Auto option, enable all renditions for adaptive selection
        levels[i].enabled = true;
      } else if (selected.index == i) {
        levels[i].enabled = true;
      } else {
        levels[i].enabled = false;
      }
    }
  }

  update() {
    let levels = this.player().qualityLevels();
    let selection = levels.selectedIndex;
    this.selected(this.options_.index == selection);
  }
}

export default SourceMenuItem;
